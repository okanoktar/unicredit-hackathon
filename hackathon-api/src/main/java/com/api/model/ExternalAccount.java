package com.api.model;

import java.util.List;

public class ExternalAccount {
	
	private String id;
	private String accountName;
	private String holderName;
	private String iban;
	private String bic;
	private String type;
	private String currency;
	private String availableBalance;
	private String currentBalance;
	private String lastUpdate;
	private String numberOfTransactionsOnScreen;
	private List<ExternalAccountTransaction> transactions;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public String getHolderName() {
		return holderName;
	}
	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}
	public String getIban() {
		return iban;
	}
	public void setIban(String iban) {
		this.iban = iban;
	}
	public String getBic() {
		return bic;
	}
	public void setBic(String bic) {
		this.bic = bic;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getAvailableBalance() {
		return availableBalance;
	}
	public void setAvailableBalance(String availableBalance) {
		this.availableBalance = availableBalance;
	}
	public String getCurrentBalance() {
		return currentBalance;
	}
	public void setCurrentBalance(String currentBalance) {
		this.currentBalance = currentBalance;
	}
	public String getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public String getNumberOfTransactionsOnScreen() {
		return numberOfTransactionsOnScreen;
	}
	public void setNumberOfTransactionsOnScreen(String numberOfTransactionsOnScreen) {
		this.numberOfTransactionsOnScreen = numberOfTransactionsOnScreen;
	}
	public List<ExternalAccountTransaction> getTransactions() {
		return transactions;
	}
	public void setTransactions(List<ExternalAccountTransaction> transactions) {
		this.transactions = transactions;
	}
	
	
	
}
