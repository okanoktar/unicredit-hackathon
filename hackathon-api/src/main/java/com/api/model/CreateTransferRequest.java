package com.api.model;

import java.math.BigDecimal;

public class CreateTransferRequest {

	private String transactionId;
	private BigDecimal amount;
	private String currency;
	private String cause;
	private Long requestedExecutionDate;
	private IbanName recipient;
	private IbanName sender;
	
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getCause() {
		return cause;
	}
	public void setCause(String cause) {
		this.cause = cause;
	}
	public IbanName getRecipient() {
		return recipient;
	}
	public void setRecipient(IbanName recipient) {
		this.recipient = recipient;
	}
	public IbanName getSender() {
		return sender;
	}
	public void setSender(IbanName sender) {
		this.sender = sender;
	}
	public Long getRequestedExecutionDate() {
		return requestedExecutionDate;
	}
	public void setRequestedExecutionDate(Long requestedExecutionDate) {
		this.requestedExecutionDate = requestedExecutionDate;
	}
	
	
}
