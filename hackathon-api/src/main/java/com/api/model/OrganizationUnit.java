package com.api.model;

public class OrganizationUnit {

	private String organizationBIC;
	private String name;
	
	public String getOrganizationBIC() {
		return organizationBIC;
	}
	public void setOrganizationBIC(String organizationBIC) {
		this.organizationBIC = organizationBIC;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
