package com.api.model;

public class Account {

	private String id;
	private String holder;
	private String currency;
	private String accountType;
	private String name;
	private ArrangementIdentifier arrangementIdentifier;
	private OrganizationUnit managingOrganzationUnit;
	private Balance balance;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getHolder() {
		return holder;
	}
	public void setHolder(String holder) {
		this.holder = holder;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ArrangementIdentifier getArrangementIdentifier() {
		return arrangementIdentifier;
	}
	public void setArrangementIdentifier(ArrangementIdentifier arrangementIdentifier) {
		this.arrangementIdentifier = arrangementIdentifier;
	}
	public OrganizationUnit getManagingOrganzationUnit() {
		return managingOrganzationUnit;
	}
	public void setManagingOrganzationUnit(OrganizationUnit managingOrganzationUnit) {
		this.managingOrganzationUnit = managingOrganzationUnit;
	}
	public Balance getBalance() {
		return balance;
	}
	public void setBalance(Balance balance) {
		this.balance = balance;
	}
	
	
}
