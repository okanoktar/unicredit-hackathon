package com.api.model;

public class IbanName {

	private String iban;
	private String name;
	
	public IbanName() {
		
	}
	
	public IbanName(String iban, String name) {
		this.iban = iban;
		this.name = name;
	}
	
	public String getIban() {
		return iban;
	}
	public void setIban(String iban) {
		this.iban = iban;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	
}
