package com.api.model;

import java.math.BigDecimal;

public class Balance {

	private BigDecimal postedAmount;
	private BigDecimal availableBalance;
	
	public BigDecimal getPostedAmount() {
		return postedAmount;
	}
	public void setPostedAmount(BigDecimal postedAmount) {
		this.postedAmount = postedAmount;
	}
	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}
	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}
	
	
}
