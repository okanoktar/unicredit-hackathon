package com.api.task;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.api.service.BatchApi;

@Component
public class ScheduledTasks {

	private static final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

	@Autowired
	BatchApi batchApi;

	@Scheduled(fixedRate = 10000)
	public void reportCurrentTime() {

		batchApi.createOffers();

		log.info("The time is now {}", dateFormat.format(new Date()));
	}
}
