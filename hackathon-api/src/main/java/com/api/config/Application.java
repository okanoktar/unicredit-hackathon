package com.api.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@ComponentScan(basePackages= {"com.api", "com.api.repository", "com.demo"})
@EnableJpaRepositories("com.api.repository") 
@EntityScan( basePackages = {"com.api.service.model"} )
@EnableRetry
@EnableScheduling
public class Application {
	
	
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
