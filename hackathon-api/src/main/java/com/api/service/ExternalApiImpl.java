package com.api.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.api.model.ExternalAccount;
import com.api.model.ExternalAccountResponse;
import com.api.util.ApiConstants;
import com.api.util.RestTemplateBuilder;

@Service
public class ExternalApiImpl implements ExternalApi {

	private RestTemplate restTemplate = RestTemplateBuilder.getRestTemplate();
	
	@Override
	public List<ExternalAccount> getCustomerAccounts(String customerId) {

		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(ApiConstants.URL + "/external/v1/accounts")
				.queryParam("customerId", customerId);

		
		ExternalAccountResponse response = restTemplate.getForObject(builder.build().toUri(), ExternalAccountResponse.class);

		List<ExternalAccount> accountList = Arrays.asList(response.accounts);
		
		return accountList;
	}

}
