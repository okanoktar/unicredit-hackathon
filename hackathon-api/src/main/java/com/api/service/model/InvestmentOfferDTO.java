package com.api.service.model;

import java.math.BigDecimal;

import com.api.model.Account;
import com.api.model.ExternalAccount;

public class InvestmentOfferDTO {

	private Long id;
	
	private String customerId;
	
	private String offerType;
	
	private ExternalAccount sourceAccount;
	
	private Account targetAccount;
	
	private BigDecimal amount;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getOfferType() {
		return offerType;
	}

	public void setOfferType(String offerType) {
		this.offerType = offerType;
	}

	public ExternalAccount getSourceAccount() {
		return sourceAccount;
	}

	public void setSourceAccount(ExternalAccount sourceAccount) {
		this.sourceAccount = sourceAccount;
	}

	public Account getTargetAccount() {
		return targetAccount;
	}

	public void setTargetAccount(Account targetAccount) {
		this.targetAccount = targetAccount;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	
	
	
	
}
