package com.api.service;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.model.ExternalAccount;
import com.api.repository.InvestmentAccountRepository;
import com.api.service.model.InvestmentAccount;
import com.api.service.model.InvestmentOffer;

@Service
public class BatchApiImpl implements BatchApi {

	@Autowired
	InvestmentAccountRepository investmentAccountRepository;

	@Autowired
	ExternalApi externalApi;

	@Autowired
	InvestmentOfferService investmentOfferService;

	private final static int IDLE_DAY_COUNT = 10;
	
	private final static BigDecimal MIN_THRESHOLD_AMOUNT = new BigDecimal(100);

	@Override
	public void createOffers() {
		// TODO Auto-generated method stub

		Iterable<InvestmentAccount> invAccounts = investmentAccountRepository.findAll();

		for (InvestmentAccount invAccount : invAccounts) {
			List<ExternalAccount> externalAccounts = externalApi.getCustomerAccounts(invAccount.getCustomerId());

			for (ExternalAccount externalAccount : externalAccounts) {
				processExternalAccount(invAccount, externalAccount);
			}
		}

	}

	private void processExternalAccount(InvestmentAccount invAccount, ExternalAccount externalAccount) {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

		try {
			Date lastUpdateDate = format.parse(externalAccount.getLastUpdate());

			long diff = new Date().getTime() - lastUpdateDate.getTime();
			System.out.println("Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));

			Long diffDays = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
			if (diffDays > IDLE_DAY_COUNT && new BigDecimal(externalAccount.getAvailableBalance()).compareTo(MIN_THRESHOLD_AMOUNT) > 0) {

				if (!investmentOfferService.hasPendingOffers(invAccount.getCustomerId(), "TIME_DEPOSIT")) {
					createOffer(invAccount, externalAccount);
				}
			}

			// boolean result = Days.daysBetween(new DateTime(lastUpdateDate), new
			// DateTime()).isGreaterThan(Days.days(30));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createOffer(InvestmentAccount invAccount, ExternalAccount externalAccount) {
		InvestmentOffer offer = new InvestmentOffer();
		offer.setOfferType("TIME_DEPOSIT");
		offer.setCustomerId(invAccount.getCustomerId());
		offer.setTargetAccountId(invAccount.getAccountId());
		offer.setSourceAccountId(externalAccount.getId());
		offer.setAmount(new BigDecimal(externalAccount.getAvailableBalance()));
		offer.setStatus("PENDING");
		investmentOfferService.createOffer(offer);
	}

}
