package com.api.service;

import java.util.List;

import com.api.model.Account;

public interface AccountApi {

	public Account getAccount(String accountId);
	public List<Account> getCustomerAccounts(String customerId);
	
}
