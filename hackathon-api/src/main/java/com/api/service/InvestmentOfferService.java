package com.api.service;

import java.util.List;

import com.api.service.model.InvestmentOffer;
import com.api.service.model.InvestmentOfferDTO;

public interface InvestmentOfferService {

	void createOffer(InvestmentOffer offer);
	InvestmentOfferDTO getOffer(Long offerId);
	List<InvestmentOfferDTO> getOffers(String customerId);
	List<InvestmentOfferDTO> getPendingOffers(String customerId);
	Boolean hasPendingOffers(String customerId, String offerType);
	
	void approveOffer(Long offerId);
	
}
