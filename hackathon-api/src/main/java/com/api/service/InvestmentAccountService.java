package com.api.service;

import com.api.service.model.InvestmentAccount;

public interface InvestmentAccountService {

	void addDefaultSavingAccount(String customerId, String accountId);
	InvestmentAccount getDefaultSavingAccount(String customerId);
	
}
