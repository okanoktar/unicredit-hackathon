package com.api.service;


import com.api.model.CreateTransferRequest;
import com.api.model.DomesticTransferResponse;


public interface TransferService {
	
	DomesticTransferResponse transferMoney(CreateTransferRequest transfer);
	

	
}
