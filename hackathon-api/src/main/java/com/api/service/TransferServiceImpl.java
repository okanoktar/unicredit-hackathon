package com.api.service;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.api.model.CreateTransferRequest;
import com.api.model.DomesticTransferResponse;
import com.api.util.ApiConstants;
import com.api.util.RestTemplateBuilder;

@Service
public class TransferServiceImpl implements TransferService {

	private RestTemplate restTemplate = RestTemplateBuilder.getRestTemplate();
	
	@Override
	public DomesticTransferResponse transferMoney(CreateTransferRequest transfer) {

		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(ApiConstants.URL + "/payments/v1/domesticTransfers");

		DomesticTransferResponse response = null;
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		//HttpEntity<CreateTransferRequest> entity = new HttpEntity<CreateTransferRequest>(transfer,headers);
		
		try {
			restTemplate.postForObject(builder.build().toUri(), transfer, DomesticTransferResponse.class);
		} catch (RestClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return response;
	}
	
	
	/*@Override
	public Transfer createTransfer(
					String amount,
					String currency,
					String cause,
					String requestedExecutionDate,
					String recipientIban,
					String recipientName,
					String senderIban,
					String senderName
					) throws Exception{
		
		Transfer resp=null;
		
		try {
			resp=new Transfer();
			resp.setAmount(amount);
			resp.setCause(cause);
			resp.setChargesAmount("0");
			resp.setChargesType(null);
			resp.setCurrency(currency);
			resp.setImmediate("true");
			resp.setIssueDate("1504450433000");
			resp.setRecipientIban(recipientIban);
			resp.setRecipientName(recipientName);
			resp.setRequestedExecutionDate("1504450433000");
			resp.setSenderIban(senderIban);
			resp.setSenderName(senderName);
			resp.setStatusCode("01");
			resp.setStatusMessage("Completed");
			resp.setTransactionId("-5e8c42d5:15e44e23cb3:-7dc6");
			
			
		}
		catch(Exception e) {
			throw e;
		}
		
		
		
		
		return resp;
		
	}*/
	
}
