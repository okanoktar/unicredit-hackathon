package com.api.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.api.model.Account;
import com.api.util.ApiConstants;
import com.api.util.RestTemplateBuilder;

@Service
public class AccountApiImpl implements AccountApi {

	private static String BANK_ID = "29000";
	private RestTemplate restTemplate = RestTemplateBuilder.getRestTemplate();

	public Account getAccount(String accountId) {

		Map<String, String> params = new HashMap<String, String>();
		params.put("bankId", BANK_ID);
		params.put("accountId", accountId);

		Account account = restTemplate.getForObject(
				ApiConstants.URL + "/accounts/v1/banks/{bankId}/accounts/{accountId}", Account.class, params);

		return account;
	}

	public List<Account> getCustomerAccounts(String customerId) {

		Map<String, String> params = new HashMap<String, String>();
		params.put("bankId", BANK_ID);

		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(ApiConstants.URL + "/accounts/v1/banks/{bankId}/accounts")
				.queryParam("customerId", customerId);

		Account[] accounts = restTemplate.getForObject(builder.buildAndExpand(params).toUri(), Account[].class);

		List<Account> accountList = Arrays.asList(accounts);

		for (Account account : accountList) {
			Account accountWithBalance = getAccount(account.getId());
			account.setBalance(accountWithBalance.getBalance());
		}

		return accountList;

	}

}
