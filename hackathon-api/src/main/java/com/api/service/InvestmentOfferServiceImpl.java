package com.api.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.model.Account;
import com.api.model.CreateTransferRequest;
import com.api.model.ExternalAccount;
import com.api.model.IbanName;
import com.api.repository.InvestmentOfferRepository;
import com.api.service.model.InvestmentOffer;
import com.api.service.model.InvestmentOfferDTO;

@Service
public class InvestmentOfferServiceImpl implements InvestmentOfferService {

	@Autowired
	InvestmentOfferRepository repository;
	
	@Autowired
	AccountApi accountApi;
	
	@Autowired
	ExternalApi externalApi;
	
	@Autowired
	TransferService transferService;
	
	@Override
	public List<InvestmentOfferDTO> getOffers(String customerId) {
		List<InvestmentOffer> investmentOffers = repository.findByCustomerId(customerId);
		
		List<InvestmentOfferDTO> investmentOfferDTOs = new ArrayList<InvestmentOfferDTO>();
		
		for (InvestmentOffer investmentOffer : investmentOffers) {
			InvestmentOfferDTO dto = new InvestmentOfferDTO();
			dto = getOffer(investmentOffer.getId());
			investmentOfferDTOs.add(dto);
		}
		
		return investmentOfferDTOs;
	}

	@Override
	public void createOffer(InvestmentOffer offer) {
		offer.setOfferType("TIME_DEPOSIT");
		repository.save(offer);
	}

	@Override
	public InvestmentOfferDTO getOffer(Long offerId) {
		InvestmentOffer investmentOffer = repository.findOne(offerId);
		
		InvestmentOfferDTO dto = new InvestmentOfferDTO();
		dto.setId(investmentOffer.getId());
		dto.setCustomerId(investmentOffer.getCustomerId());
		dto.setOfferType(investmentOffer.getOfferType());
		dto.setAmount(investmentOffer.getAmount());
		
		Account account = accountApi.getAccount(investmentOffer.getTargetAccountId());
		List<ExternalAccount> externalAccounts = externalApi.getCustomerAccounts(investmentOffer.getCustomerId());
		
		ExternalAccount externalAccount = externalAccounts.stream().filter((acc) -> acc.getId().equals(investmentOffer.getSourceAccountId())).findAny().get();
		
		dto.setTargetAccount(account);
		dto.setSourceAccount(externalAccount);
		
		return dto;
	}
	
	public Boolean isOfferExist(String customerId, String offerType, String status) {
		List<InvestmentOffer> offers = repository.findByCustomerIdAndOfferTypeAndStatus(customerId, offerType, status);
		return !offers.isEmpty();
	}

	@Override
	public Boolean hasPendingOffers(String customerId, String offerType) {
		return isOfferExist(customerId, offerType, "PENDING");
	}

	@Override
	public List<InvestmentOfferDTO> getPendingOffers(String customerId) {
		List<InvestmentOffer> investmentOffers = repository.findByCustomerIdAndStatus(customerId, "PENDING");
		
		List<InvestmentOfferDTO> investmentOfferDTOs = new ArrayList<InvestmentOfferDTO>();
		
		for (InvestmentOffer investmentOffer : investmentOffers) {
			InvestmentOfferDTO dto = new InvestmentOfferDTO();
			dto = getOffer(investmentOffer.getId());
			investmentOfferDTOs.add(dto);
		}
		
		return investmentOfferDTOs;
	}

	@Override
	public void approveOffer(Long offerId) {
		
		InvestmentOfferDTO offerDTO = getOffer(offerId);
		
		CreateTransferRequest transferRequest = new CreateTransferRequest();
		
		IbanName sender = new IbanName(offerDTO.getSourceAccount().getIban(), "John Wayn");
		IbanName receiver = new IbanName(offerDTO.getTargetAccount().getArrangementIdentifier().getIban(), "John Wayn");
		
		transferRequest.setSender(sender);
		transferRequest.setRecipient(receiver);
		//transferRequest.setAmount(offerDTO.getAmount());
		transferRequest.setAmount(new BigDecimal(1));
		transferRequest.setCurrency(offerDTO.getSourceAccount().getCurrency());
		transferRequest.setCause("money transfer");
		//transferRequest.setTransactionId(UUID.randomUUID().toString());
		
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		transferRequest.setRequestedExecutionDate(timestamp.getTime());
		
		transferService.transferMoney(transferRequest);
		
		
	}
	
	

}
