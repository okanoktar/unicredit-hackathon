package com.api.service;

import java.util.List;

import com.api.model.ExternalAccount;

public interface ExternalApi {
	
	public List<ExternalAccount> getCustomerAccounts(String customerId);
	
}
