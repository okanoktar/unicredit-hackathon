package com.api.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.repository.InvestmentAccountRepository;
import com.api.service.model.InvestmentAccount;

@Service
@Transactional
public class InvesmentAccountServiceImpl implements InvestmentAccountService {
	
	@Autowired
	InvestmentAccountRepository repository;
	
	@Override
	public void addDefaultSavingAccount(String customerId, String accountId) {
		InvestmentAccount investmentAccount = repository.findOneByCustomerId(customerId);
		
		repository.delete(investmentAccount);
		
		InvestmentAccount invAccount = new InvestmentAccount();
		
		invAccount.setCustomerId(customerId);
		invAccount.setAccountId(accountId);
		invAccount.setIsDefault(true);
		
		repository.save(invAccount);
		
	}

	@Override
	public InvestmentAccount getDefaultSavingAccount(String customerId) {
		return repository.findOneByCustomerId(customerId);
	}

}
