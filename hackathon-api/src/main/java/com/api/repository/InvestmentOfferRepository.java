package com.api.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.api.service.model.InvestmentOffer;


public interface InvestmentOfferRepository extends CrudRepository<InvestmentOffer, Long> {

	List<InvestmentOffer> findByCustomerId(String customerId);
	
	List<InvestmentOffer> findByCustomerIdAndStatus(String customerId, String status);
	
	List<InvestmentOffer> findByCustomerIdAndOfferTypeAndStatus(String customerId, String offerType, String status);
	
}
