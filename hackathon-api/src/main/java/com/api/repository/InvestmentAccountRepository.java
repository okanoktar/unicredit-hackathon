package com.api.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.api.service.model.InvestmentAccount;

public interface InvestmentAccountRepository extends CrudRepository<InvestmentAccount, Long> {

	List<InvestmentAccount> findByCustomerId(String customerId);
	InvestmentAccount findOneByCustomerId(String customerId);
	
}
