package com.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.api.service.BatchApi;

@RestController
@RequestMapping("/batch")
public class SavingAccountController {

	@Autowired
	BatchApi batchApiService;
	
	@RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> run() {
		
		batchApiService.createOffers();
		
    		return new ResponseEntity<>(HttpStatus.OK);
    }
	
}
