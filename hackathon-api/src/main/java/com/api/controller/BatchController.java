package com.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.api.controller.model.CreateSavingAccountRequest;
import com.api.service.InvestmentAccountService;
import com.api.service.model.InvestmentAccount;

@RestController
@RequestMapping("/customers/{customerId}/savingAccounts")
public class BatchController {

	@Autowired
	InvestmentAccountService investmentAccountService;
	
	@RequestMapping(method = RequestMethod.GET)
    public InvestmentAccount getSavingAccounts(@PathVariable String customerId) {
		return investmentAccountService.getDefaultSavingAccount(customerId);
    }
	
	@RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> saveSaving(@RequestBody CreateSavingAccountRequest request) {
		
		investmentAccountService.addDefaultSavingAccount(request.getCustomerId(), request.getAccountId());
		
    		return new ResponseEntity<>(HttpStatus.CREATED);
    }
	
}
