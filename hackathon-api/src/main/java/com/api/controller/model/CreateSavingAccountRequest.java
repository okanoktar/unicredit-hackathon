package com.api.controller.model;

import com.api.service.model.InvestmentAccount;

public class CreateSavingAccountRequest {

	private String customerId;

	private String accountId;
	
	public InvestmentAccount asInvesmentAccount() {
		InvestmentAccount invAccount = new InvestmentAccount();
		invAccount.setCustomerId(customerId);
		invAccount.setAccountId(accountId);
		return invAccount;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	
	
}
