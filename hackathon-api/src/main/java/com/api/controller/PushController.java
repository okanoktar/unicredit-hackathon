package com.api.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.demo.push.request.Notification;
import com.demo.push.request.PushNotificationMessage;
import com.demo.push.service.PushNotificationService;
import com.demo.web.model.SendPushRequest;

@RestController
public class PushController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	PushNotificationService pushNotificationService;

	@RequestMapping(value = "/push", method = RequestMethod.POST)
	public ResponseEntity<?> sendOfferPush(@RequestBody SendPushRequest request)  {
		
		Notification notification = new Notification("new investment offer for you !", "We are recommending you a new investment strategy.");
		
		PushNotificationMessage message = new PushNotificationMessage.MessageBuilder()
			    .toToken(request.getDeviceToken()) // single android/ios device
			    .notification(notification)
			    .addData("offerId",request.getOfferId())
			    .build();
		
		pushNotificationService.sendPush(message);
		
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

}
