package com.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
 
import com.api.model.Account;
import com.api.service.AccountApi;

@RestController
@RequestMapping("/customers/{customerId}/accounts")
public class AccountController {

	@Autowired
	AccountApi accountApi;
	
	@RequestMapping(method = RequestMethod.GET)
    public List<Account> getCustomerAccounts(@PathVariable String customerId) {
		return accountApi.getCustomerAccounts(customerId);
    }
	
	@RequestMapping(method = RequestMethod.GET, value = "/{accountId}")
    public Account getCustomerAccount(@PathVariable String accountId) {
		return accountApi.getAccount(accountId);
    }
}
