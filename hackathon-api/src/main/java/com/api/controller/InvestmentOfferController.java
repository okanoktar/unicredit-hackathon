package com.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.api.service.InvestmentOfferService;
import com.api.service.model.InvestmentOfferDTO;

@RestController
@RequestMapping("/customers/{customerId}/offers")
public class InvestmentOfferController {

	@Autowired
	InvestmentOfferService investmentOfferService;

	@RequestMapping(method = RequestMethod.GET)
	public List<InvestmentOfferDTO> getCustomer(@PathVariable String customerId) {
		return investmentOfferService.getPendingOffers(customerId);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{offerId}")
	public InvestmentOfferDTO getOffer(@PathVariable Long offerId) {
		return investmentOfferService.getOffer(offerId);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/{offerId}/approve")
	public ResponseEntity<?> approveOffer(@PathVariable Long offerId) {
		
		investmentOfferService.approveOffer(offerId);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
}
