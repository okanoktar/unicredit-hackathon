package com.api.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.demo.web.model.CreateFbUserRequest;

@RestController
public class SocialUserController {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
    @RequestMapping(value = "/fb-user", method = RequestMethod.POST)
    public ResponseEntity<?> createFbUser(@RequestBody CreateFbUserRequest token) {
    
    		logger.info(token.toString());
    		return new ResponseEntity<>(HttpStatus.CREATED);
    }
    
    
    
}
