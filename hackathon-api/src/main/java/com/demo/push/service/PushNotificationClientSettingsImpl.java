package com.demo.push.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PushNotificationClientSettingsImpl implements PushNotificationClientSettings {

	@Value("${fcm.api.url}")
	private String apiUrl;
	
	@Value("${fcm.api.key}")
	private String apiKey;
	
	@Override
	public String getApiUrl() {
		return apiUrl;
	}

	@Override
	public String getApiKey() {
		return apiKey;
	}

}
