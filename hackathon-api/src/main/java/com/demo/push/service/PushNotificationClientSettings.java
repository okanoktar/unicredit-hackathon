package com.demo.push.service;

public interface PushNotificationClientSettings {

	public String getApiUrl();
    public String getApiKey();
    
}
