package com.demo.push.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Notification {

	private String title;
    private String body;
    private String icon;
    
    public Notification(String title, String body) {
		this.title = title;
		this.body = body;
	}
    
    @JsonProperty("title")
	public String getTitle() {
		return title;
	}
    
    @JsonProperty("body")
	public String getBody() {
		return body;
	}
    
    @JsonProperty("icon")
	public String getIcon() {
		return icon;
	}
    
    
}
