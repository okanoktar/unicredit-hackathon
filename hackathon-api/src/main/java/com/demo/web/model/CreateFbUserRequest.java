package com.demo.web.model;

public class CreateFbUserRequest {

	private  String accessToken;
	private String refreshToken;
	private String providerId;
	private String providerUserId;
	
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public String getRefreshToken() {
		return refreshToken;
	}
	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}
	public String getProviderId() {
		return providerId;
	}
	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}
	public String getProviderUserId() {
		return providerUserId;
	}
	public void setProviderUserId(String providerUserId) {
		this.providerUserId = providerUserId;
	}
	
	@Override
	public String toString() {
		return "CREATE FB REQUEST = { providerId : " + providerId +
				" providerUserId :" + providerUserId +
				" accessToken :" + accessToken +
				" refreshToken : " + refreshToken + " }";
	}
	
}
