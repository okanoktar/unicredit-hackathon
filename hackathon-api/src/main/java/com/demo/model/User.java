package com.demo.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class User {

	@Id
	@GeneratedValue
	private Long id;
	
	private String providerId;
	
	private String providerUserId;
	
	private String profileImageUrl;
	
	@OneToMany(mappedBy="user", fetch = FetchType.EAGER)
	private Set<BankCustomer> bankCustomers;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProviderId() {
		return providerId;
	}

	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}

	public String getProviderUserId() {
		return providerUserId;
	}

	public void setProviderUserId(String providerUserId) {
		this.providerUserId = providerUserId;
	}

	public String getProfileImageUrl() {
		return profileImageUrl;
	}

	public void setProfileImageUrl(String profileImageUrl) {
		this.profileImageUrl = profileImageUrl;
	}

	public Set<BankCustomer> getBankCustomers() {
		return bankCustomers;
	}

	public void setBankCustomers(Set<BankCustomer> bankCustomers) {
		this.bankCustomers = bankCustomers;
	}

	
	
	
	
}
